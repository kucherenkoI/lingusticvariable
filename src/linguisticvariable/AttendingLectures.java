package linguisticvariable;

import numbers.TrapezoidalNumber;

/**
 * Created by Igor on 16.11.2014.
 */
public class AttendingLectures {

    public static final int MIN_VALUE = 0;
    public static final int MAX_VALUE = 100;

    public static final TrapezoidalNumber LOW = new TrapezoidalNumber(MIN_VALUE, MAX_VALUE);
    public static final TrapezoidalNumber MIDDLE = new TrapezoidalNumber(MIN_VALUE, MAX_VALUE);
    public static final TrapezoidalNumber HIGH = new TrapezoidalNumber(MIN_VALUE, MAX_VALUE);

    static {
        final int leftBottomValue  = 0;
        final int leftTopValue     = 20;
        final int rightBottomValue = 50;
        final int rightTopValue    = 30;
        LOW.setValue(leftBottomValue, leftTopValue, rightBottomValue, rightTopValue);
    }
    static {
        int leftBottomValue  = 30;
        int leftTopValue     = 50;
        int rightTopValue    = 70;
        int rightBottomValue = 80;
        MIDDLE.setValue(leftBottomValue, leftTopValue, rightBottomValue, rightTopValue);
    }
    static {
        int leftBottomValue  = 70;
        int leftTopValue     = 90;
        int rightBottomValue = 100;
        int rightTopValue    = 100;
        HIGH.setValue(leftBottomValue, leftTopValue, rightBottomValue, rightTopValue);
    }

}
