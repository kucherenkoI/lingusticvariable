package calculating;

import linguisticvariable.AttendingLectures;
import linguisticvariable.LaboratoryWork;
import linguisticvariable.ReadTheBook;
import numbers.FunctionalNumber;
import org.junit.Assert;
import org.junit.Test;

import static java.lang.Math.max;

/**
 * Created by Igor on 08.12.2014.
 */
public class VariablesState {

    public static class AttendingLecturesState {

        private static double mLow;
        private static double mMiddle;
        private static double mHigh;

        public static double getLow() {
            return mLow;
        }

        public static void setLow(double low) {
            ValidValueChecker.check(low);
            mLow = low;
        }

        public static double getMiddle() {
            return mMiddle;
        }

        public static void setMiddle(double middle) {
            ValidValueChecker.check(middle);
            mMiddle = middle;
        }

        public static double getHigh() {
            return mHigh;
        }

        public static void setHigh(double high) {
            ValidValueChecker.check(high);
            mHigh = high;
        }

       /* public static FunctionalNumber getMaxNumber() {
            double compareValue = max(mLow, max(mMiddle, mHigh));
            if(compareValue == mLow) return AttendingLectures.LOW;
            if(compareValue == mMiddle) return  AttendingLectures.MIDDLE;
            if(compareValue == mHigh) return  AttendingLectures.HIGH;
            throw new IllegalStateException("Not max value from AttendingLectures");
        }*/

    }

    public static class LaboratoryWorkState {

        private static double mSeveral;
        private static double mAlmostAll;
        private static double mAll;

        public static double getSeveral() {
            return mSeveral;
        }

        public static void setSeveral(double several) {
            ValidValueChecker.check(several);
            mSeveral = several;
        }

        public static double getAlmostAll() {
            return mAlmostAll;
        }

        public static void setAlmostAll(double almostAll) {
            ValidValueChecker.check(almostAll);
            mAlmostAll = almostAll;
        }

        public static double getAll() {
            return mAll;
        }

        public static void setAll(double all) {
            ValidValueChecker.check(all);
            mAll = all;
        }

      /*  public static FunctionalNumber getMaxNumber() {
            double compareValue = max(mSeveral, max(mAlmostAll, mAll));
            if(compareValue == mSeveral) return LaboratoryWork.SEVERAL;
            if(compareValue == mAlmostAll) return  LaboratoryWork.ALMOST_ALL;
            if(compareValue == mAll) return  LaboratoryWork.ALL;
            throw new IllegalStateException("Not max value from AttendingLectures");
        }*/

    }

    public static class ReadTheBookState {

        private static double mNoRead;
        private static double mNotMany;
        private static double mEnough;
        private static double mMany;

        public static double getNoRead() {
            return mNoRead;
        }

        public static void setNoRead(double noRead) {
            ValidValueChecker.check(noRead);
            mNoRead = noRead;
        }

        public static double getNotMany() {
            return mNotMany;
        }

        public static void setNotMany(double notMany) {
            ValidValueChecker.check(notMany);
            mNotMany = notMany;
        }

        public static double getEnough() {
            return mEnough;
        }

        public static void setEnough(double enough) {
            ValidValueChecker.check(enough);
            mEnough = enough;
        }

        public static double getMany() {
            return mMany;
        }

        public static void setMany(double many) {
            ValidValueChecker.check(many);
            mMany = many;
        }

       /* public static FunctionalNumber getMaxNumber() {
            double compareValue = max(mNoRead, max(mNotMany, max(mEnough, mMany)));
            if(compareValue == mNoRead) return ReadTheBook.NO_READ;
            if(compareValue == mNotMany) return ReadTheBook.NOT_MANY;
            if(compareValue == mEnough) return ReadTheBook.ENOUGH;
            if(compareValue == mMany) return  ReadTheBook.MANY;
            throw new IllegalStateException("Not max value from AttendingLectures");
        }*/

    }

    private static class ValidValueChecker {

        private static void check(double value) throws IllegalArgumentException {
            if (value < 0.0 || value > 1.0) throw new IllegalArgumentException("Value not [0,1]!!");
        }

    }


    public static void setStates(double lectures, double books, double labs) {
        VariablesState.AttendingLecturesState.setLow(AttendingLectures.LOW.function(lectures));
        VariablesState.AttendingLecturesState.setMiddle(AttendingLectures.MIDDLE.function(lectures));
        VariablesState.AttendingLecturesState.setHigh(AttendingLectures.HIGH.function(lectures));

        VariablesState.ReadTheBookState.setNoRead(ReadTheBook.NO_READ.function(books));
        VariablesState.ReadTheBookState.setEnough(ReadTheBook.ENOUGH.function(books));
        VariablesState.ReadTheBookState.setNotMany(ReadTheBook.NOT_MANY.function(books));
        VariablesState.ReadTheBookState.setMany(ReadTheBook.MANY.function(books));

        VariablesState.LaboratoryWorkState.setSeveral(LaboratoryWork.SEVERAL.function(labs));
        VariablesState.LaboratoryWorkState.setAlmostAll(LaboratoryWork.ALMOST_ALL.function(labs));
        VariablesState.LaboratoryWorkState.setAll(LaboratoryWork.ALL.function(labs));
    }

}
