package calculating.util;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Igor on 13.12.2014.
 */
public class MathUtil {

    private static final int MIN_AMOUNT_OF_COMPARE_VALUE = 2;

    public static double min(double[] array) {
        if(array.length < MIN_AMOUNT_OF_COMPARE_VALUE) throw new IllegalArgumentException("few amount of values for compare");
        final int indexOfFirstValue = 0;
        double firstCompareValue = array[indexOfFirstValue];
        final int indexOfSecondCompareValue = 1;
        double result = firstCompareValue;
        for(int step = indexOfSecondCompareValue; step < array.length; step++) {
            result = Math.min(result, array[step]);
        }
        return result;
    }

    public static double max(double[] array) {
        if(array.length < MIN_AMOUNT_OF_COMPARE_VALUE) throw new IllegalArgumentException("few amount of values for compare");
        final int indexOfFirstValue = 0;
        double firstCompareValue = array[indexOfFirstValue];
        final int indexOfSecondCompareValue = 1;
        double result = firstCompareValue;
        for(int step = indexOfSecondCompareValue; step < array.length; step++) {
            result = Math.max(result, array[step]);
        }
        return result;
    }

    @Test
    public void testMin() {
        Assert.assertEquals(2.0, min(new double[]{2.01, 4.5, 4.9, 2.0}), 0.0);
    }

    @Test
    public void testMax() {
        Assert.assertEquals(4.9, max(new double[]{2.01, 4.5, 4.9, 2.0}), 0.0);
    }

}
