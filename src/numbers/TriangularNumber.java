package numbers;

import functions.Functional;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import static numbers.Constants.MAX_M_VALUE;

/**
 * Created by Igor on 16.11.2014.
 */
public class TriangularNumber extends FunctionalNumber {


    private int leftValue;
    private int middleValue;
    private int rightValue;

    public TriangularNumber(int minValue, int maxValue) {
        super(minValue, maxValue);
    }

    public void setValue(int leftValue, int  middleValue, int rightValue) {
        this.leftValue = leftValue;
        this.middleValue = middleValue;
        this.rightValue = rightValue;
    }

    @Override
    public double function(double x) {
        if(leftValue == minValue && middleValue == minValue && x < rightValue) return MAX_M_VALUE;

        if(x <= leftValue) return 0;
        if(leftValue <= x && x <= middleValue) return ((x - leftValue) / (middleValue - leftValue));
        if(middleValue <= x && x <= rightValue) return ((rightValue - x) / (rightValue - middleValue));
        if(rightValue <= x) return 0;
        throw new IllegalArgumentException();
    }
}
