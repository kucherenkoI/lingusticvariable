package chart;


import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

/**
 * Created by Igor on 22.11.2014.
 */
public class Chart extends ApplicationFrame {

    public Chart(final String title, XYDataset dataset, String nameChart) {
        super(title);
        final JFreeChart chart = createChart(dataset, nameChart);
        final ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
        setContentPane(chartPanel);
    }

    public JFreeChart createChart(final XYDataset dataset, String nameChart) {

        final JFreeChart chart = ChartFactory.createXYLineChart(
                nameChart,
                "X",
                "M",
                dataset,
                PlotOrientation.VERTICAL,
                true,
                true,
                false
        );

        return chart;

    }

    public static void draw(XYSeriesCollection dataSet, String name) {
        final Chart demo = new Chart(name, dataSet, "Chart");
        demo.pack();
        RefineryUtilities.centerFrameOnScreen(demo);
        demo.setVisible(true);
    }

    public static void draw(XYSeriesCollection dataSet, String name, String nameChart) {
        final Chart demo = new Chart(name, dataSet, nameChart);
        demo.pack();
        RefineryUtilities.centerFrameOnScreen(demo);
        demo.setVisible(true);
    }


}

