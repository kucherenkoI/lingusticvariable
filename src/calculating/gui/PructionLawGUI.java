package calculating.gui;

import calculating.ProductionLaw;
import calculating.VariablesState;
import linguisticvariable.AttendingLectures;
import linguisticvariable.LaboratoryWork;
import linguisticvariable.ReadTheBook;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Igor on 13.12.2014.
 */
public class PructionLawGUI {
    private JButton a40Book3LabButton;
    private JButton a50Book4LabButton;
    private JButton a60Book4LabButton;
    private JButton a70Book2LabButton;
    private JButton a80Book6LabButton;
    private JPanel rootPanel;

    public static void main(String[] args) {
        JFrame frame = new JFrame("PructionLawGUI");
        frame.setContentPane(new PructionLawGUI().rootPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocation(300,300);
        frame.pack();
        frame.setVisible(true);
    }

    public PructionLawGUI() {
        a40Book3LabButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                final int lectures = 40;
                final int book = 3;
                final int lab = 7;

                VariablesState.setStates(lectures, book, lab);
                ProductionLaw.drawLawResult();

            }
        });
        a50Book4LabButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                final int lectures = 50;
                final int book = 4;
                final int lab = 8;

                VariablesState.setStates(lectures, book, lab);
                ProductionLaw.drawLawResult();

            }
        });
        a60Book4LabButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                final int lectures = 60;
                final int book = 4;
                final int lab = 5;

                VariablesState.setStates(lectures, book, lab);
                ProductionLaw.drawLawResult();

            }
        });
        a70Book2LabButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                final int lectures = 70;
                final int book = 2;
                final int lab = 3;

                VariablesState.setStates(lectures, book, lab);
                ProductionLaw.drawLawResult();

            }
        });
        a80Book6LabButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                final int lectures = 80;
                final int book = 6;
                final int lab = 8;

                VariablesState.setStates(lectures, book, lab);
                ProductionLaw.drawLawResult();

            }
        });
    }
}
