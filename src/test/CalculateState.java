package test;

import calculating.VariablesState;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Igor on 08.12.2014.
 */
public class CalculateState {

    @Test
    public void testVariableStateNormal() {
        VariablesState.AttendingLecturesState state = new VariablesState.AttendingLecturesState();
        state.setHigh(0.89);
        Assert.assertEquals(0.89, state.getHigh(), 0.00);
    }

    @Test()
    public void testVariableStateError() throws IllegalArgumentException {
        VariablesState.AttendingLecturesState state = new VariablesState.AttendingLecturesState();
        state.setHigh(1.89);
    }

}
