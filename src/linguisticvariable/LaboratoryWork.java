package linguisticvariable;
import numbers.TriangularNumber;

/**
 * Created by Igor on 16.11.2014.
 */
public class LaboratoryWork {

    public static final int MIN_VALUE = 0;
    public static final int MAX_VALUE = 8;

    public static final TriangularNumber SEVERAL = new TriangularNumber(MIN_VALUE, MAX_VALUE);
    public static final TriangularNumber ALMOST_ALL = new TriangularNumber(MIN_VALUE, MAX_VALUE);
    public static final TriangularNumber ALL = new TriangularNumber(MIN_VALUE, MAX_VALUE);

    static {
        int leftValue   = 0;
        int middleValue = 2;
        int rightValue  = 4;
        SEVERAL.setValue(leftValue, middleValue, rightValue);
    }
    static {
        int leftValue   = 2;
        int middleValue = 5;
        int rightValue  = 8;
        ALMOST_ALL.setValue(leftValue, middleValue, rightValue);
    }
    static {
        int leftValue   = 6;
        int middleValue = 8;
        int rightValue  = 8;
        ALL.setValue(leftValue, middleValue, rightValue);
    }

}
