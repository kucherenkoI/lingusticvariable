package operations;

import functions.Functional;
import linguisticvariable.AttendingLectures;
import numbers.Constants;
import numbers.FunctionalNumber;

import static java.lang.Math.max;
import static java.lang.Math.min;
import static numbers.Constants.MAX_M_VALUE;

/**
 * Created by Igor on 22.11.2014.
 */
public class Operations {

   public static double conjunction(double x, Functional firstFunction, Functional secondFunction) {
       return min(firstFunction.function(x), secondFunction.function(x));
   }

   public static double disjunction(double x, Functional firstFunction, Functional secondFunction) {
       return max(firstFunction.function(x), secondFunction.function(x));
   }

   public static double negation(double x, Functional functional) {
       return MAX_M_VALUE - functional.function(x);
   }

}
