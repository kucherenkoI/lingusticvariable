package operations;

import chart.Chart;
import functions.Functional;
import numbers.FunctionalNumber;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 * Created by Igor on 23.11.2014.
 */
public class OperationTools {

    public static void conjunction(FunctionalNumber firstFunctional, FunctionalNumber secondFunctional) {
        final XYSeries series = new XYSeries("Result");
        for(int step = firstFunctional.minValue; step <= firstFunctional.maxValue; step++) {
            series.add(step, Operations.conjunction(step, firstFunctional, secondFunctional));
        }
        final XYSeriesCollection dataSet = new XYSeriesCollection();
        dataSet.addSeries(series);
        Chart.draw(dataSet, "Result");
    }
    public static void disjunction(FunctionalNumber firstFunctional, FunctionalNumber secondFunctional) {
        final XYSeries series = new XYSeries("Result");
        for(int step = firstFunctional.minValue; step <= firstFunctional.maxValue; step++) {
            series.add(step, Operations.disjunction(step, firstFunctional, secondFunctional));
        }
        final XYSeriesCollection dataSet = new XYSeriesCollection();
        dataSet.addSeries(series);
        Chart.draw(dataSet, "Result");
    }
    public static void negative(FunctionalNumber firstFunctional) {
        final XYSeries series = new XYSeries("Result");
        for(int step = firstFunctional.minValue; step <= firstFunctional.maxValue; step++) {
            series.add(step, Operations.negation(step, firstFunctional));
        }
        final XYSeriesCollection dataSet = new XYSeriesCollection();
        dataSet.addSeries(series);
        Chart.draw(dataSet, "Result");
    }

}
