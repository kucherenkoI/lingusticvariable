package functions;

/**
 * Created by Igor on 22.11.2014.
 */
public interface Functional {

    public double function(double x);

}
