package numbers;

import functions.Functional;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import static numbers.Constants.*;

/**
 * Created by Igor on 16.11.2014.
 */
public class TrapezoidalNumber extends FunctionalNumber {


    private int leftBottomValue;
    private int leftTopValue;
    private int rightBottomValue;
    private int rightTopValue;


    public TrapezoidalNumber(int minValue, int maxValue) {
        super(minValue, maxValue);
    }

    public void setValue(int leftBottomValue, int leftTopValue, int rightBottomValue, int rightTopValue) {
        this.leftBottomValue  = leftBottomValue;
        this.leftTopValue     = leftTopValue;
        this.rightBottomValue = rightBottomValue;
        this.rightTopValue    = rightTopValue;
    }

    @Override
    public double function(double x) {

        if(leftTopValue == minValue && leftBottomValue == minValue && x < rightTopValue) return MAX_M_VALUE;

        if(x <= leftBottomValue) return 0;

        if(leftBottomValue <= x && x <= leftTopValue) {
            int denominator = leftTopValue - leftBottomValue;
            if(denominator == 0) return MIN_M_VALUE;
            return ((x - leftBottomValue) / denominator);
        }

        if(leftTopValue <= x && x <= rightTopValue) {
            return MAX_M_VALUE;
        }

        if(rightTopValue <= x && x <= rightBottomValue) {
            int denominator = rightBottomValue - rightTopValue;
            if(denominator == 0) return MAX_M_VALUE;
            return ((rightBottomValue - x) / denominator);
        }

        if(x >= rightBottomValue) return 0;

        throw new IllegalArgumentException();
    }
}
