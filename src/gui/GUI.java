package gui;

import linguisticvariable.AttendingLectures;
import linguisticvariable.LaboratoryWork;
import linguisticvariable.Rating;
import linguisticvariable.ReadTheBook;
import operations.OperationTools;
import org.jfree.ui.RefineryUtilities;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.plaf.basic.BasicBorders;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/**
 * Created by Igor on 23.11.2014.
 */
public class GUI {

    private JPanel root;
    private JButton низкоеButton;
    private JButton высокоеButton;
    private JButton среднееButton;
    private JButton несколькоButton;
    private JButton почтиВсеButton;
    private JButton всеButton;
    private JButton достаточноButton;
    private JButton немногоButton;
    private JButton неЧиталисьButton;
    private JButton многоButton;
    private JButton оченьВысокийButton;
    private JButton среднийButton;
    private JButton высокийButton;
    private JButton низкийButton;
    private JComboBox operationComboBox;
    private JComboBox variableComboBox;
    private JCheckBox secondCheckBox;
    private JCheckBox firstCheckBox;
    private JCheckBox thirdCheckBox;
    private JCheckBox fourthCheckBox;
    private JPanel operationPanel;
    private JButton выполнитьButton;


    public GUI() {

        firstCheckBox.setText("");
        secondCheckBox.setText("");
        thirdCheckBox.setText("");
        fourthCheckBox.setText("");

        operationComboBox.addItem("Конъюнкция");
        operationComboBox.addItem("Дизъюнкция");
        operationComboBox.addItem("Отрицание");

        variableComboBox.addItem("Посещение лекций");
        variableComboBox.addItem("Сдано лабораторных");
        variableComboBox.addItem("Рейтинг");
        variableComboBox.addItem("Прочитано книг");


        среднееButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                AttendingLectures.MIDDLE.drawChart();
            }
        });
        низкоеButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                AttendingLectures.LOW.drawChart();
            }
        });
        высокоеButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                AttendingLectures.HIGH.drawChart();
            }
        });
        почтиВсеButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                LaboratoryWork.ALMOST_ALL.drawChart();
            }
        });
        несколькоButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                LaboratoryWork.SEVERAL.drawChart();
            }
        });
        всеButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                LaboratoryWork.ALL.drawChart();
            }
        });
        неЧиталисьButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ReadTheBook.NO_READ.drawChart();
            }
        });
        достаточноButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ReadTheBook.ENOUGH.drawChart();
            }
        });
        немногоButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ReadTheBook.NOT_MANY.drawChart();
            }
        });
        многоButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ReadTheBook.MANY.drawChart();
            }
        });
        низкийButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Rating.LOW.drawChart();
            }
        });
        среднийButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Rating.MIDDLE.drawChart();
            }
        });
        высокийButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Rating.HIGH.drawChart();
            }
        });
        оченьВысокийButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Rating.VERY_HIGH.drawChart();
            }
        });

        operationComboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                switch (operationComboBox.getSelectedIndex()) {

                }
            }
        });

        variableComboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

                firstCheckBox.setVisible(true);
                secondCheckBox.setVisible(true);
                thirdCheckBox.setVisible(true);
                fourthCheckBox.setVisible(true);
                root.updateUI();
                operationPanel.updateUI();

                switch (variableComboBox.getSelectedIndex()) {
                    case 0: {
                        firstCheckBox.setText("Низкое");
                        secondCheckBox.setText("Среднее");
                        thirdCheckBox.setText("Высокое");
                        fourthCheckBox.setVisible(false);
                        operationPanel.updateUI();
                        root.updateUI();
                        break;
                    }
                    case 1: {
                        firstCheckBox.setText("Несколько");
                        secondCheckBox.setText("Почти все");
                        thirdCheckBox.setText("Все");
                        fourthCheckBox.setVisible(false);
                        operationPanel.updateUI();
                        root.updateUI();
                        break;
                    }
                    case 2: {
                        firstCheckBox.setText("Низкий");
                        secondCheckBox.setText("Средний");
                        thirdCheckBox.setText("Высокий");
                        fourthCheckBox.setText("Очень высокий");
                        operationPanel.updateUI();
                        root.updateUI();
                        break;
                    }
                    case 3: {
                        firstCheckBox.setText("Не читались");
                        secondCheckBox.setText("Не много");
                        thirdCheckBox.setText("Достаточно");
                        fourthCheckBox.setText("Много");
                        operationPanel.updateUI();
                        root.updateUI();
                        break;
                    }
                }
            }
        });


        выполнитьButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

                if(variableComboBox.getSelectedIndex() == 0) {
                    if(operationComboBox.getSelectedIndex() == 0) {
                        if(firstCheckBox.isSelected() && secondCheckBox.isSelected()) {
                            OperationTools.conjunction(AttendingLectures.LOW, AttendingLectures.MIDDLE);
                        }
                        if(firstCheckBox.isSelected() && thirdCheckBox.isSelected()) {
                            OperationTools.conjunction(AttendingLectures.LOW, AttendingLectures.HIGH);
                        }
                        if(secondCheckBox.isSelected() && thirdCheckBox.isSelected()) {
                            OperationTools.conjunction(AttendingLectures.HIGH, AttendingLectures.MIDDLE);
                        }
                    }
                }
                if(variableComboBox.getSelectedIndex() == 1) {
                    if(operationComboBox.getSelectedIndex() == 0) {
                        if(firstCheckBox.isSelected() && secondCheckBox.isSelected()) {
                            OperationTools.conjunction(LaboratoryWork.SEVERAL, LaboratoryWork.ALMOST_ALL);
                        }
                        if(firstCheckBox.isSelected() && thirdCheckBox.isSelected()) {
                            OperationTools.conjunction(LaboratoryWork.SEVERAL, LaboratoryWork.ALL);
                        }
                        if(secondCheckBox.isSelected() && thirdCheckBox.isSelected()) {
                            OperationTools.conjunction(LaboratoryWork.ALMOST_ALL, LaboratoryWork.ALL);
                        }
                    }
                }
                if(variableComboBox.getSelectedIndex() == 2) {
                    if(operationComboBox.getSelectedIndex() == 0) {
                        if(firstCheckBox.isSelected() && secondCheckBox.isSelected()) {
                            OperationTools.conjunction(Rating.LOW, Rating.MIDDLE);
                        }
                        if(firstCheckBox.isSelected() && thirdCheckBox.isSelected()) {
                            OperationTools.conjunction(Rating.LOW, Rating.HIGH);
                        }
                        if(firstCheckBox.isSelected() && fourthCheckBox.isSelected()) {
                            OperationTools.conjunction(Rating.LOW, Rating.VERY_HIGH);
                        }
                        if(secondCheckBox.isSelected() && fourthCheckBox.isSelected()) {
                            OperationTools.conjunction(Rating.MIDDLE, Rating.VERY_HIGH);
                        }
                        if(secondCheckBox.isSelected() && thirdCheckBox.isSelected()) {
                            OperationTools.conjunction(Rating.MIDDLE, Rating.HIGH);
                        }
                        if(thirdCheckBox.isSelected() && fourthCheckBox.isSelected()) {
                            OperationTools.conjunction(Rating.HIGH, Rating.VERY_HIGH);
                        }
                    }
                }
                if(variableComboBox.getSelectedIndex() == 3) {
                    if(operationComboBox.getSelectedIndex() == 0) {
                        if(firstCheckBox.isSelected() && secondCheckBox.isSelected()) {
                            OperationTools.conjunction(ReadTheBook.NO_READ, ReadTheBook.NOT_MANY);
                        }
                        if(firstCheckBox.isSelected() && thirdCheckBox.isSelected()) {
                            OperationTools.conjunction(ReadTheBook.NO_READ, ReadTheBook.ENOUGH);
                        }
                        if(firstCheckBox.isSelected() && fourthCheckBox.isSelected()) {
                            OperationTools.conjunction(ReadTheBook.NO_READ, ReadTheBook.MANY);
                        }
                        if(secondCheckBox.isSelected() && fourthCheckBox.isSelected()) {
                            OperationTools.conjunction(ReadTheBook.NOT_MANY, ReadTheBook.MANY);
                        }
                        if(secondCheckBox.isSelected() && thirdCheckBox.isSelected()) {
                            OperationTools.conjunction(ReadTheBook.NOT_MANY, ReadTheBook.ENOUGH);
                        }
                        if(thirdCheckBox.isSelected() && fourthCheckBox.isSelected()) {
                            OperationTools.conjunction(ReadTheBook.ENOUGH, ReadTheBook.MANY);
                        }
                    }
                }

                if(variableComboBox.getSelectedIndex() == 0) {
                    if(operationComboBox.getSelectedIndex() == 1) {
                        if(firstCheckBox.isSelected() && secondCheckBox.isSelected()) {
                            OperationTools.disjunction(AttendingLectures.LOW, AttendingLectures.MIDDLE);
                        }
                        if(firstCheckBox.isSelected() && thirdCheckBox.isSelected()) {
                            OperationTools.disjunction(AttendingLectures.LOW, AttendingLectures.HIGH);
                        }
                        if(secondCheckBox.isSelected() && thirdCheckBox.isSelected()) {
                            OperationTools.disjunction(AttendingLectures.HIGH, AttendingLectures.MIDDLE);
                        }
                    }
                }
                if(variableComboBox.getSelectedIndex() == 1) {
                    if(operationComboBox.getSelectedIndex() == 1) {
                        if(firstCheckBox.isSelected() && secondCheckBox.isSelected()) {
                            OperationTools.disjunction(LaboratoryWork.SEVERAL, LaboratoryWork.ALMOST_ALL);
                        }
                        if(firstCheckBox.isSelected() && thirdCheckBox.isSelected()) {
                            OperationTools.disjunction(LaboratoryWork.SEVERAL, LaboratoryWork.ALL);
                        }
                        if(secondCheckBox.isSelected() && thirdCheckBox.isSelected()) {
                            OperationTools.disjunction(LaboratoryWork.ALMOST_ALL, LaboratoryWork.ALL);
                        }
                    }
                }
                if(variableComboBox.getSelectedIndex() == 2) {
                    if(operationComboBox.getSelectedIndex() == 1) {
                        if(firstCheckBox.isSelected() && secondCheckBox.isSelected()) {
                            OperationTools.disjunction(Rating.LOW, Rating.MIDDLE);
                        }
                        if(firstCheckBox.isSelected() && thirdCheckBox.isSelected()) {
                            OperationTools.disjunction(Rating.LOW, Rating.HIGH);
                        }
                        if(firstCheckBox.isSelected() && fourthCheckBox.isSelected()) {
                            OperationTools.disjunction(Rating.LOW, Rating.VERY_HIGH);
                        }
                        if(secondCheckBox.isSelected() && fourthCheckBox.isSelected()) {
                            OperationTools.disjunction(Rating.MIDDLE, Rating.VERY_HIGH);
                        }
                        if(secondCheckBox.isSelected() && thirdCheckBox.isSelected()) {
                            OperationTools.disjunction(Rating.MIDDLE, Rating.HIGH);
                        }
                        if(thirdCheckBox.isSelected() && fourthCheckBox.isSelected()) {
                            OperationTools.disjunction(Rating.HIGH, Rating.VERY_HIGH);
                        }
                    }
                }
                if(variableComboBox.getSelectedIndex() == 3) {
                    if(operationComboBox.getSelectedIndex() == 1) {
                        if(firstCheckBox.isSelected() && secondCheckBox.isSelected()) {
                            OperationTools.disjunction(ReadTheBook.NO_READ, ReadTheBook.NOT_MANY);
                        }
                        if(firstCheckBox.isSelected() && thirdCheckBox.isSelected()) {
                            OperationTools.disjunction(ReadTheBook.NO_READ, ReadTheBook.ENOUGH);
                        }
                        if(firstCheckBox.isSelected() && fourthCheckBox.isSelected()) {
                            OperationTools.disjunction(ReadTheBook.NO_READ, ReadTheBook.MANY);
                        }
                        if(secondCheckBox.isSelected() && fourthCheckBox.isSelected()) {
                            OperationTools.disjunction(ReadTheBook.NOT_MANY, ReadTheBook.MANY);
                        }
                        if(secondCheckBox.isSelected() && thirdCheckBox.isSelected()) {
                            OperationTools.disjunction(ReadTheBook.NOT_MANY, ReadTheBook.ENOUGH);
                        }
                        if(thirdCheckBox.isSelected() && fourthCheckBox.isSelected()) {
                            OperationTools.disjunction(ReadTheBook.ENOUGH, ReadTheBook.MANY);
                        }
                    }
                }
                if(variableComboBox.getSelectedIndex() == 0) {
                    if(operationComboBox.getSelectedIndex() == 2) {
                        if(firstCheckBox.isSelected()) {
                            OperationTools.negative(AttendingLectures.LOW);
                        }
                        if(thirdCheckBox.isSelected()) {
                            OperationTools.negative(AttendingLectures.HIGH);
                        }
                        if(secondCheckBox.isSelected()) {
                            OperationTools.negative(AttendingLectures.MIDDLE);
                        }
                    }
                }
                if(variableComboBox.getSelectedIndex() == 1) {
                    if(operationComboBox.getSelectedIndex() == 2) {
                        if(firstCheckBox.isSelected()) {
                            OperationTools.negative(LaboratoryWork.SEVERAL);
                        }
                        if(thirdCheckBox.isSelected()) {
                            OperationTools.negative(LaboratoryWork.ALL);
                        }
                        if(secondCheckBox.isSelected()) {
                            OperationTools.negative(LaboratoryWork.ALMOST_ALL);
                        }
                    }
                }
                if(variableComboBox.getSelectedIndex() == 2) {
                    if(operationComboBox.getSelectedIndex() == 2) {
                        if(firstCheckBox.isSelected()) {
                            OperationTools.negative(Rating.LOW);
                        }
                        if(thirdCheckBox.isSelected()) {
                            OperationTools.negative(Rating.HIGH);
                        }
                        if(secondCheckBox.isSelected()) {
                            OperationTools.negative(Rating.MIDDLE);
                        }
                        if(fourthCheckBox.isSelected()) {
                            OperationTools.negative(Rating.VERY_HIGH);
                        }
                    }
                }
                if(variableComboBox.getSelectedIndex() == 3) {
                    if(operationComboBox.getSelectedIndex() == 2) {
                        if(firstCheckBox.isSelected()) {
                            OperationTools.negative(ReadTheBook.NO_READ);
                        }
                        if(secondCheckBox.isSelected()) {
                            OperationTools.negative(ReadTheBook.NOT_MANY);
                        }
                        if(thirdCheckBox.isSelected()) {
                            OperationTools.negative(ReadTheBook.ENOUGH);
                        }
                        if(fourthCheckBox.isSelected()) {
                            OperationTools.negative(ReadTheBook.MANY);
                        }
                    }
                }


            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("GUI");
        frame.setContentPane(new GUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        RefineryUtilities.centerFrameOnScreen(frame);
        frame.setVisible(true);
    }
}
