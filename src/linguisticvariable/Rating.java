package linguisticvariable;

import numbers.TrapezoidalNumber;

/**
 * Created by Igor on 16.11.2014.
 */
public class Rating {

    public static final int MIN_VALUE = 0;
    public static final int MAX_VALUE = 10;

    public static final TrapezoidalNumber LOW = new TrapezoidalNumber(MIN_VALUE, MAX_VALUE);
    public static final TrapezoidalNumber MIDDLE = new TrapezoidalNumber(MIN_VALUE, MAX_VALUE);
    public static final TrapezoidalNumber HIGH = new TrapezoidalNumber(MIN_VALUE, MAX_VALUE);
    public static final TrapezoidalNumber VERY_HIGH = new TrapezoidalNumber(MIN_VALUE, MAX_VALUE);

    static {
        int leftBottomValue  = 0;
        int leftTopValue     = 0;
        int rightTopValue    = 2;
        int rightBottomValue = 3;
        LOW.setValue(leftBottomValue, leftTopValue, rightBottomValue, rightTopValue);
    }
    static {
        int leftBottomValue  = 1;
        int leftTopValue     = 3;
        int rightTopValue = 4;
        int rightBottomValue    = 6;
        MIDDLE.setValue(leftBottomValue, leftTopValue, rightBottomValue, rightTopValue);
    }
    static {
        int leftBottomValue  = 5;
        int leftTopValue     = 6;
        int rightTopValue = 7;
        int rightBottomValue    = 9;
        HIGH.setValue(leftBottomValue, leftTopValue, rightBottomValue, rightTopValue);
    }
    static {
        int leftBottomValue  = 8;
        int leftTopValue     = 9;
        int rightTopValue = 10;
        int rightBottomValue    = 10;
        VERY_HIGH.setValue(leftBottomValue, leftTopValue, rightBottomValue, rightTopValue);
    }

}
