package calculating.testVariable;

import linguisticvariable.AttendingLectures;
import linguisticvariable.LaboratoryWork;
import linguisticvariable.ReadTheBook;
import org.junit.Test;

/**
 * Created by Igor on 13.12.2014.
 */
public class TestVariable {


    @Test
    public void test60_4_5() {
        System.out.println(AttendingLectures.MIDDLE.function(60));
        System.out.println(ReadTheBook.NOT_MANY.function(4));
        System.out.println(LaboratoryWork.ALMOST_ALL.function(5));
    }
}
