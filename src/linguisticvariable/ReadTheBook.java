package linguisticvariable;

import numbers.TriangularNumber;

/**
 * Created by Igor on 16.11.2014.
 */
public class ReadTheBook {

    public static final int MIN_VALUE = 0;
    public static final int MAX_VALUE = 10;

    public static final TriangularNumber NO_READ = new TriangularNumber(MIN_VALUE, MAX_VALUE);
    public static final TriangularNumber NOT_MANY = new TriangularNumber(MIN_VALUE, MAX_VALUE);
    public static final TriangularNumber ENOUGH = new TriangularNumber(MIN_VALUE, MAX_VALUE);
    public static final TriangularNumber MANY = new TriangularNumber(MIN_VALUE, MAX_VALUE);

    static {
        int leftValue   = 0;
        int middleValue = 0;
        int rightValue  = 1;
        NO_READ.setValue(leftValue, middleValue, rightValue);
    }
    static {
        int leftValue   = 0;
        int middleValue = 2;
        int rightValue  = 4;
        NOT_MANY.setValue(leftValue, middleValue, rightValue);
    }
    static {
        int leftValue   = 1;
        int middleValue = 4;
        int rightValue  = 9;
        ENOUGH.setValue(leftValue, middleValue, rightValue);
    }
    static {
        int leftValue   = 5;
        int middleValue = 10;
        int rightValue  = 10;
        MANY.setValue(leftValue, middleValue, rightValue);
    }
}
