package calculating;

import calculating.util.MathUtil;
import chart.Chart;
import linguisticvariable.AttendingLectures;
import linguisticvariable.LaboratoryWork;
import linguisticvariable.Rating;
import linguisticvariable.ReadTheBook;
import numbers.FunctionalNumber;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Igor on 09.12.2014.
 */
public class ProductionLaw {

    public static void drawLawResult() {

        double firstCutValue = getCutValueOfFirstLaw();
        double secondCutValue = getCutValueOfSecondLaw();
        double thirdCutValue = getCutValueOfThirdLaw();
        double fourthCutValue = getCutValueOfFourthLaw();

        double approximation = 10;

        double integralStep = (Rating.MAX_VALUE - Rating.MIN_VALUE) / approximation;

        double denumSum = 0.0;
        double numSum = 0.0;

        double valueInFirstStep = 0.0;
        double valueInLastStep = 0.0;


        final XYSeries series = new XYSeries("Law");
        for(int step = Rating.MIN_VALUE; step <= Rating.MAX_VALUE; step++) {

            double originalMu1 = Rating.LOW.function(step);
            double cutMu1 = getCutMu(originalMu1, firstCutValue);

            double originalMu2 = Rating.MIDDLE.function(step);
            double cutMu2 = getCutMu(originalMu2, secondCutValue);

            double originalMu3 = Rating.HIGH.function(step);
            double cutMu3 = getCutMu(originalMu3, thirdCutValue);

            double originalMu4 = Rating.VERY_HIGH.function(step);
            double cutMu4 = getCutMu(originalMu4, fourthCutValue);

            double resultMu = MathUtil.max(new double[]{cutMu1, cutMu2, cutMu3, cutMu4});

            if(step == Rating.MIN_VALUE) {
                valueInFirstStep = resultMu;
            }
            if(step == Rating.MAX_VALUE) {
                valueInLastStep = resultMu;
            }

            numSum += step * resultMu;
            denumSum += resultMu;

            series.add(step, resultMu);
        }


        double denumIntegral = integralStep *((valueInFirstStep + valueInLastStep) / 2 + denumSum);
        double numIntegral = integralStep *((valueInFirstStep + valueInLastStep) / 2 + numSum);


        final XYSeriesCollection dataSet = new XYSeriesCollection();
        dataSet.addSeries(series);

        Chart.draw(dataSet, "Лаба 4", "Центр тяжести равен: " + String.valueOf(numIntegral / denumIntegral));

    }

    private static double getCutMu(double original, double cutValue) {
        if(original > cutValue) return cutValue;
        return original;
    }

    private static double getCutValueOfFirstLaw() {
        double firstValue = VariablesState.AttendingLecturesState.getLow();
        double secondValue = VariablesState.ReadTheBookState.getNoRead();
        double thirdValue = VariablesState.LaboratoryWorkState.getSeveral();
        double cutValue = MathUtil.min(new double[]{firstValue, secondValue, thirdValue});
        return cutValue;
    }

    private static double getCutValueOfSecondLaw() {
        double firstValue = VariablesState.AttendingLecturesState.getMiddle();
        double secondValue = VariablesState.ReadTheBookState.getNotMany();
        double thirdValue = VariablesState.LaboratoryWorkState.getAlmostAll();
        double cutValue = MathUtil.min(new double[]{firstValue, secondValue, thirdValue});
        return cutValue;
    }

    private static double getCutValueOfThirdLaw() {
        double secondValue = VariablesState.ReadTheBookState.getEnough();
        double thirdValue = VariablesState.LaboratoryWorkState.getAll();
        double cutValue = MathUtil.min(new double[]{secondValue, thirdValue});
        return cutValue;
    }

    private static double getCutValueOfFourthLaw() {
        double firstValue = VariablesState.AttendingLecturesState.getHigh();
        double secondValue = VariablesState.ReadTheBookState.getMany();
        double thirdValue = VariablesState.LaboratoryWorkState.getAll();
        double cutValue = MathUtil.min(new double[]{firstValue, secondValue, thirdValue});
        return cutValue;
    }


}
