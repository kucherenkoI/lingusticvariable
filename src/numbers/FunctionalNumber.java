package numbers;

import chart.Chart;
import functions.Functional;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.RefineryUtilities;

/**
 * Created by Igor on 22.11.2014.
 */
public abstract class FunctionalNumber implements Functional {

    public final int minValue;
    public final int maxValue;

    protected String name = "chart";

    protected FunctionalNumber(int minValue, int maxValue) {
        if(minValue >= maxValue) throw new IllegalArgumentException();
        if(maxValue <= minValue) throw new IllegalArgumentException();
        this.minValue = minValue;
        this.maxValue = maxValue;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract double function(double x);

    public void drawChart(){
        Chart.draw(getDataSet(), name);
    }


    public XYSeriesCollection getDataSet() {
        final XYSeries series = new XYSeries(name);
        for(int step = minValue; step <= maxValue; step++) {
            series.add(step, function(step));
        }
        final XYSeriesCollection dataSet = new XYSeriesCollection();
        dataSet.addSeries(series);
        return dataSet;
    }

}
